# React app by Truffle Box Example

## Getting started
1. (only windows) run `npm install -g windows-build-tools`
2. run `npm install -g truffle`
3. run `npm install`
4. run `truffle develop`
5. run `compile` inside development console 
6. run `migrate` inside development console 
7. run `npm run start` on anotehr console.
8. Open http://localhost:3000 in the browser
