import Web3 from 'web3'

let getWeb3 = new Promise(function(resolve, reject) {
  // Wait for loading completion to avoid race conditions with web3 injection timing.
  window.addEventListener('load', function() {
    let results
    let web3 = window.web3

    // Fallback to localhost if no web3 injection. We've configured this to
    // use the development console's port by default.
    const provider = new Web3.providers.HttpProvider('http://127.0.0.1:9545')
    // const provider = web3.currentProvider

    web3 = new Web3(provider)

    results = {
      web3: web3
    }

    resolve(results)
  })
})

export default getWeb3
