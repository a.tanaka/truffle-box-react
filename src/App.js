import React, { Component } from 'react'
import contract from 'truffle-contract'
import SimpleStorageContract from '../build/contracts/SimpleStorage.json'
import getWeb3 from './utils/getWeb3'

import './css/oswald.css'
import './css/open-sans.css'
import './css/pure-min.css'
import './App.css'

class App extends Component {
  
  /**
   * Creates an instance of App.
   * @param {*} props
   * @memberof App
   */
  constructor(props) {
    super(props);

    this.state = {
      storageValue: 0,
      web3: null,
      accounts: [],
      myAccount: '',
      deleteAccount: '',
      inputValue: 0,
      instance: null,
    };
  }
  
  /**
   * The first true life cycle method called
   *
   * @memberof App
   */
  componentWillMount() {
    // Get network provider and web3 instance.
    // See utils/getWeb3 for more info.
    getWeb3.then((results) => {
      // set state
      this.setState({ web3: results.web3});

      // Instantiate contract once web3 provided.
      this.instantiateContract();
    })
    .catch(() => console.log('Error finding web3.'))
  }
  
  /**
   * Instantiate & call contract
   *
   * @memberof App
   */
  async instantiateContract() {
    /*
     * SMART CONTRACT EXAMPLE
     *
     * Normally these functions would be called in the context of a
     * state management library, but for convenience I've placed them here.
     */
     try {
      const simpleStorage = contract(SimpleStorageContract);
      simpleStorage.setProvider(this.state.web3.currentProvider);

      // Get contract instanve
      const instance = await simpleStorage.deployed();
      this.setState({ instance: instance });

      // Get accounts.
      const accounts = await this.getAccounts();
      this.setState({ accounts: accounts });
      this.setState({ myAccount: accounts[0]});
      this.setState({ deleteAccount: accounts[0]});

      // Stores a given value, 5 by default.
      this.updateStore(5);
      this.getStorageValue(accounts[0]);

     } catch (error) {
      alert(error);
     }
  }
  
  /**
   * Get Accounts
   *
   * @returns
   * @memberof App
   */
  getAccounts() {
    return new Promise((resolve, reject) => {
      this.state.web3.eth.getAccounts((error, accounts) => {
        if (error) {
          reject(error);
        } else {
          resolve(accounts);
        }
      });
    });
  }
  
  /**
   * Update store data
   *
   * @param {*} value
   * @memberof App
   */
  async updateStore(value) {
    try {
      // Stores a given value
      await this.state.instance.set(value, {from: this.state.myAccount});

    } catch (error) {
      alert(error);
    }
  }

  
  /**
   * Get storage value from node
   *
   * @memberof App
   */
  async getStorageValue(myAccount) {
      // Get the value from the contract to prove it worked.
      // Result is a BigNumber object
      // c: coefficient, e: exponent, s: sign
      const callResult = await this.state.instance.get.call(myAccount);

      // Update state with the result.
      this.setState({ storageValue: callResult.toNumber() });
  }
  
  /**
   * Check if transaction is mined
   *
   * @param {*} txHash
   * @returns
   * @memberof App
   */
  getTxReceipt(txHash) {
    return new Promise((resolve, reject) => {
      // ToDo: Get TransactionReceipt
      resolve();
    });
  }
  
  /**
   * Handle input change event
   *
   * @param {*} event
   * @memberof App
   */
  handleChangeInputValue(event) {
    this.setState({ inputValue: event.target.value });
  }

  /**
   * Handle submit event
   *
   * @param {*} event
   * @memberof App
   */
  handleUpdateSubmit(event) {
    // ToDo: Stores a given value

    event.preventDefault();
  }

  /**
   * Handle my account changing
   *
   * @param {*} event
   * @memberof App
   */
  handleChangeMyAccount(event) {
    const myAccount = event.target.value;

    this.setState({ myAccount });
    this.getStorageValue(myAccount);
  }

  /**
   * Handle delete account changing
   *
   * @param {*} event
   * @memberof App
   */
  handleChangeDeleteAccount(event) {
    this.setState({ deleteAccount: event.target.value });
  }

  /**
   * Handle submit event
   *
   * @memberof App
   */
  async handleDeleteSubmit() {
    try {
      // ToDo: Delete store data

      // Get the value from the contract to prove it worked.
      this.getStorageValue(this.state.myAccount);

    } catch (error) {
      alert(error);
    }
  }

  /**
   * Render html
   *
   * @returns
   * @memberof App
   */
  render() {
    var options = this.state.accounts.map(function(account) {
      return <option value={account} key={account}>{account}</option>;
    });
    return (
      <div className="App">
        <nav className="navbar pure-menu pure-menu-horizontal">
          <a href="#" className="pure-menu-heading pure-menu-link">Truffle Box</a>
        </nav>

        <main className="container">
          <div className="pure-g">
            <div className="pure-u-1-1">
              <h2>The stored value is <span className="storage-value">{this.state.storageValue}</span></h2>
              <p>
                If your contracts compiled and migrated successfully, <br />
                below will show a stored value of 5 (by default).
              </p>
              <p>
                Your account is<br />
                <select value={this.state.myAccount} onChange={ e => this.handleChangeMyAccount(e) }>
                  {options}
                </select>
              </p>
              
              {/* <div className="control-panel">
                <div>
                  <form action="javascript:void(0)" onSubmit={ e => this.handleUpdateSubmit(e) }>
                    <input type="number" value={this.state.inputValue} onChange={ e => this.handleChangeInputValue(e) } />
                    <input type="submit" value="Update" />
                  </form>
                </div>
              </div> */}

              {/* <hr />

              <div className="control-panel">
                <div>
                  <select value={this.state.deleteAccount} onChange={ e => this.handleChangeDeleteAccount(e) }>
                    {options}
                  </select>
                </div>
                <div>
                  <form action="javascript:void(0)" onSubmit={ e => this.handleDeleteSubmit() }>
                    <input type="submit" value="Delete" />
                  </form>
                </div>
              </div> */}

            </div>
          </div>
        </main>
      </div>
    );
  }
}

export default App
