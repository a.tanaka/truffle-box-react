pragma solidity ^0.4.18;

contract SimpleStorage {
  mapping (address => uint) storedData;

  function set(uint x) public {
    storedData[msg.sender] = x;
  }

  function del(address _owner) public {
    require(_owner == msg.sender);
    delete storedData[msg.sender];
  }

  function get(address _owner) public view returns (uint) {
    return storedData[_owner];
  }
}
